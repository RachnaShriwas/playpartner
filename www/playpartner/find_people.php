<?php
require_once 'update_user_info.php';
$db = new update_user_info();

// json response array
$response = array("error" => FALSE);

if (isset($_POST['email']) && isset($_POST['cityname'])) {

    // receiving the post params
    $email = $_POST['email'];
    $cityname = $_POST['cityname'];

	//update user location in database
	db->addLocation($email, $cityname);
	
    // get the users by location
    $result = $db->findPeople($cityname, $email);

    if ($result != false) {
        // use is found
		while($row = $result->fetch_assoc()) {
			$user = array();
			
			$user["name"] = $row['name'];
			$user["sport"] = $row['sport'];
			
			array_push($response["user"], $user);
		}
		
		echo json_encode($response);
	}
	else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "No user found for this location!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email or cityname is missing!";
    echo json_encode($response);
}
?>