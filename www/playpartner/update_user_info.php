<?php

class update_user_info {

    private $conn;

    // constructor
    function __construct() {
        require_once 'connect.php';
        // connecting to database
        $db = new connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {

    }

    /**
     * Storing new user
     * returns user details
     */
    public function StoreUserInfo($name, $email, $password, $gender, $sport) {
        $stmt = $this->conn->prepare("INSERT INTO user(name, email, password, gender, sport) VALUES(?, ?, ?, ?, ?)");
        $stmt->bind_param("sssss", $name, $email, $password, $gender, $sport);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT name, email, password, gender, sport FROM user WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt-> bind_result($token2,$token3,$token4,$token5,$token6);
            while ( $stmt-> fetch() ) {
               $user["name"] = $token2;
               $user["email"] = $token3;
               $user["gender"] = $token5;
               $user["sport"] = $token6;
            }
            $stmt->close();
            return $user;
        } else {
          return false;
        }
    }

    /**
     * Get user by email and password
     */
    public function VerifyUserAuthentication($email, $password) {

        $stmt = $this->conn->prepare("SELECT name, email, password, gender, sport FROM user WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $stmt-> bind_result($token2,$token3,$token4,$token5,$token6);

            while ( $stmt-> fetch() ) {
               $user["name"] = $token2;
               $user["email"] = $token3;
               $user["password"] = $token4;
               $user["gender"] = $token5;
               $user["sport"] = $token6;
            }

            $stmt->close();
			
            // check for password equality
            if (password == $token4) {
                // user authentication details are correct
                return $user;
            }
        } else {
            return NULL;
        }
    }

    /**
     * Check user is existed or not
     */
    public function CheckExistingUser($email) {
        $stmt = $this->conn->prepare("SELECT email from user WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
	
	/**
	 * Update user location
	 */
	public function addLocation($email, $cityname) {
		$stmt = $this->conn->prepare("UPDATE user SET location = ? WHERE email = ?");
		
		$stmt->bind_param("ss", $cityname, $email);
		
		$stmt->execute();
		
		$stmt->store_result();
	}
	
	function resultToArray($result) {
		$rows = array();
		while($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}
		return $rows;
	}
	
	/**
	 * Find people and sport based on location
	 */
	public function findPeople($cityname, $email) {
		/*$stmt = $this->conn->prepare("SELECT name, sport FROM user where location = ? and email != ?");
		
		$stmt->bind_param("s", $cityname, $email);
		
		if ($stmt->execute()) {
            $stmt-> bind_result($token2,$token6);

            while ( $stmt-> fetch() ) {
               $user["name"] = $token2;
               $user["sport"] = $token6;
            }

            $stmt->close();
		}
		else{
			return NULL;
		}
		*/
		
		$query = 'SELECT name, sport FROM user where location = $cityname and email != $email';
		$result = $this->conn->query($query);
		
		if($result->num_rows > 0) {
			return $result;
		}
		else {
			return false;
		}
	}
}

?>