# PlayPartner

Play Partner is an android app for women to find women in their neighborhood for playing sports.


The app opens up in a login page. User can login (or register if she is a new user) to the app which will open the Home page. Home page has options like 'Meet People Around You, 'Team Chat' and 'Schedule'.

Meet People Around You shows list of all women and their favorite sports, which are nearby.
Team Chat lets the user chat with any other user or group of users.
Schedule shows the upcoming matches in a calendar.

Description of all the files:

LoginActivity.java : Starting activity of the app. Needs email and password for login

RegisterActivity.java : New user registration

HomeActivity.java : Shows home page of the app. Contains buttons to move to next activity

LocationActivity.java : Fetches user's current location through GPS

MeetPeopleActivity.java : Displays nearby users along with the sports they like


www/playpartner : contains PHP files for database connection and queries.
