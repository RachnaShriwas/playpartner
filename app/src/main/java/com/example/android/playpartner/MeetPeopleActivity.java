package com.example.android.playpartner;

/**
 * Created by Rachna on 04-Mar-18.
 */


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import android.widget.Toast;

public class MeetPeopleActivity extends AppCompatActivity  {

    private static final String TAG = "MeetPeopleActivity";
    private String user;
    private TextView greetingTextView, user_name, user_sport;
    private Button btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_people);

        user = getIntent().getExtras().getString("user");

        greetingTextView = (TextView) findViewById(R.id.greeting_text_view);
        btnLogOut = (Button) findViewById(R.id.logout_button);
        greetingTextView.setText("Hello "+ user);

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        JSONObject jObj = null;
        String name = "", sport = "";
        try {
            jObj = new JSONObject(getIntent().getStringExtra("jObj"));
            if(jObj != null) {
                JSONArray arr = jObj.getJSONArray("user");
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj1 = arr.getJSONObject(i);
                    name = obj1.getString("name");
                    sport = obj1.getString("sport");
                }
            }
            else {
                String errorMsg = jObj.getString("error_msg");
                Toast.makeText(getApplicationContext(),
                    errorMsg, Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        user_name = (TextView) findViewById(R.id.user_name);
        user_name.setText("Name: " + name);

        user_sport = (TextView) findViewById(R.id.user_sport);
        user_sport.setText("Sport: " + sport);
    }
}
