package com.example.android.playpartner;

/**
 * Created by Rachna on 03-Mar-18.
 */



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";

    private TextView greetingTextView;
    private Button btnLogOut, btnMeetPeople;
    private String user, email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Bundle bundle = getIntent().getExtras();
        user = bundle.getString("username");
        email = bundle.getString("email");

        greetingTextView = (TextView) findViewById(R.id.greeting_text_view);
        btnLogOut = (Button) findViewById(R.id.logout_button);
        greetingTextView.setText("Hello "+ user);

        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        btnMeetPeople = (Button) findViewById(R.id.meet_people);
        btnMeetPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), LocationActivity.class);
                i.putExtra("user", user);
                i.putExtra("email", email);
                startActivity(i);
            }
        });

    }
}


